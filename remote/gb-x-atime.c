/*
 * gb-x-reset-atime - reset atime to ctime/mtime
 * gb-x-check-atime - check if files have been used
 */

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>

static
int handle(const char *path)
{
    struct stat st;
    if (lstat(path, &st) < 0)
	return -1;
#define TvLE(tv1, tv2) \
    ((tv1).tv_sec < (tv2).tv_sec || \
     ((tv1).tv_sec == (tv2).tv_sec && \
      (tv1).tv_nsec <= (tv2).tv_nsec))
    // unused file condition
    if (TvLE(st.st_atim, st.st_ctim) && TvLE(st.st_atim, st.st_mtim))
	return 0;
#if PROG_RESET_ATIME
    st.st_atim = st.st_ctim;
    if (TvLE(st.st_mtim, st.st_ctim))
	st.st_atim = st.st_mtim;
    struct timespec times[] = { st.st_atim, st.st_mtim };
    if (utimensat(AT_FDCWD, path, times, AT_SYMLINK_NOFOLLOW) < 0)
	return -2;
#elif PROG_CHECK_ATIME
    if (puts(path) == EOF)
	return -2;
#else
#error "prog mode not set"
#endif
    return 1;
}

int main(int argc, char **argv)
{
    if (argc > 1) {
	int i;
	for (i = 1; i < argc; i++) {
	    const char *path = argv[i];
	    if (handle(path) < 0) {
		perror(path);
		return 1;
	    }
	}
	return 0;
    }
    char *line = NULL;
    size_t alloc_size = 0;
    ssize_t len;
    while ((len = getline(&line, &alloc_size, stdin)) >= 0) {
	if (len > 0 && line[len-1] == '\n')
	    line[--len] = '\0';
	if (len == 0)
	    continue;
	const char *path = line;
	if (handle(path) < 0) {
	    perror(path);
	    return 1;
	}
    }
    return 0;
}

// ex: set ts=8 sts=4 sw=4 noet:
