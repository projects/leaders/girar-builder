#!/bin/sh
#
# Functions defined in this file are used by girar-builder scripts.
#

# Source function library.
. /etc/init.d/functions

start_script_daemon()
{
	local ANNOUNCE BASENAME CMDNAME DISPNAME EXPECT FLAGS LOCKFILE MAKE_PIDFILE NICE PIDFILE STATUS SU WHICH

# Process options.
	ANNOUNCE=1
	CMDNAME=
	DISPNAME=
	EXPECT=
	LOCKFILE=
	MAKE_PIDFILE=
	NICE=0
	PIDFILE=
	SU=
	while [ "$1" != "${1##-}" -o "$1" != "${1##+}" ]; do
		case "$1" in
		--)
			shift
			break
			;;
		--announce)
			shift
			ANNOUNCE=1
			;;
		--no-announce)
			shift
			ANNOUNCE=
			;;
		--displayname)
			shift
			DISPNAME="$1"
			shift
			;;
		--expect-user)
			shift
			EXPECT="$1"
			shift
			;;
		--lockfile)
			shift
			LOCKFILE="$1"
			shift
			;;
		--background)
			shift
			MAKE_PIDFILE='--background'
			;;
		--name)
			shift
			CMDNAME="$1"
			shift
			;;
		--pidfile)
			shift
			PIDFILE="$1"
			shift
			;;
		--user|--set-user)
			shift
			SU="$1"
			[ -n "$EXPECT" ] || EXPECT="$SU"
			shift
			;;
		[-+][0-9]*)
			NICE="$1"
			shift
			;;
		*)
			echo "start_daemon: unrecognized option: $1" >&2
			return 1
			;;
		esac
	done

# We need the absolute pathname.
	if [ -z "$1" ]; then
		msg_usage "start_daemon [options]... {program}..."
		return 1
	fi
	WHICH="$(absolute "$1")" || return 1
	[ -n "$CMDNAME" ] &&
		BASENAME="$(basename "$CMDNAME")" ||
		BASENAME="$(basename "$1")" || return 1
	shift

	if [ -n "$CMDNAME" -a -z "$PIDFILE" ]; then
		echo "start_daemon: --name is set but --pidfile is not set" >&2
		return 1
	fi

	[ -n "$DISPNAME" ] || DISPNAME="$BASENAME"

# Use a safe umask
	#umask 077

# Don't export these because they may be invalid under another UID and the
# directories may be gone while the daemon is still running.
	export -n HOME TMP TMPDIR

	FLAGS="--start -N $NICE"
	[ -z "$CMDNAME" ] &&
		FLAGS="$FLAGS --exec $WHICH" ||
		FLAGS="$FLAGS --startas $WHICH --name $CMDNAME"
	[ -z "$PIDFILE" ] || FLAGS="$FLAGS --pidfile $PIDFILE"
	[ -z "$EXPECT" ] || FLAGS="$FLAGS --user $EXPECT"
	[ -z "$MAKE_PIDFILE" ] || FLAGS="$FLAGS $MAKE_PIDFILE"

# Is it running at all?
	if ! start-stop-daemon $FLAGS --test > /dev/null; then
		msg_already_running "$DISPNAME"
		passed "$BASENAME startup"
		STATUS=$?
		echo
		return $STATUS
	fi

# Announce the action.
	[ -z "$ANNOUNCE" ] || msg_starting "$DISPNAME"

# Actually start the daemon.
	if [ -z "$SU" ]; then
		initlog $INITLOG_ARGS -n "$BASENAME" -c "limited -n $BASENAME -- start-stop-daemon $FLAGS -- $*"
	else
		initlog $INITLOG_ARGS -n "$BASENAME" -c "limited -n $BASENAME -- start-stop-daemon $FLAGS --startas /bin/su -- \
			-s /bin/sh -l '$SU' -c '$WHICH $*'"
	fi
	STATUS=$?

	if [ $STATUS = 0 ]; then
		[ -z "$LOCKFILE" ] || touch "$LOCKFILE"
		[ "$BOOTUP" != verbose ] || echo -n " $DISPNAME "
		success "$BASENAME startup"
	else
		failure "$BASENAME startup"
	fi
	echo

	return $STATUS
}
