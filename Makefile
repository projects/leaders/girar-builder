DESTDIR =
datadir = /usr/share
libexecdir = /usr/libexec
localstatedir = /var/lib
sbindir = /usr/sbin
spooldir = /var/spool
sysconfdir = /etc

girar_bindir = ${libexecdir}/girar
girar_sbindir = ${sbindir}
girar_confdir = ${sysconfdir}/girar
girar_datadir = ${datadir}/girar
girar_spooldir = ${spooldir}/girar
girar_statedir = ${localstatedir}/girar
girar_hooks_dir = ${girar_datadir}/hooks
girar_templates_dir = ${girar_datadir}/templates
girar_packages_dir = ${girar_datadir}/packages.git
girar_private_dir = ${girar_datadir}/private.git
girar_public_dir = ${girar_datadir}/public.git
girar_email_dir = ${girar_statedir}/email
girar_acl_conf_dir = ${girar_confdir}/acl
girar_acl_pub_dir = ${girar_statedir}/acl.pub
girar_acl_state_dir = ${girar_statedir}/acl

VENDOR_NAME = Etersoft
EMAIL_DOMAIN = etersoft.ru
GB_USER = girar-builder
GB_GROUP = girar-builder
GB_TASKS = ${girar_spooldir}/tasks
GIRAR_ARCHIVE = /archive
GIRAR_EMAIL_ALIASES = ${girar_confdir}/aliases
GIRAR_FAKE_HOME = ${girar_datadir}/home
GIRAR_GEARS = /gears
GIRAR_HOME = /people
GIRAR_PACKAGES_LIST = ${girar_statedir}/people-packages-list
GIRAR_PEOPLE_QUEUE = ${girar_spooldir}/people
GIRAR_REPOSITORIES = ${girar_confdir}/repositories
GITWEB_URL = http://git.etersoft.ru
PACKAGES_EMAIL = ${VENDOR_NAME} Devel discussion list <devel@lists.${EMAIL_DOMAIN}>
USER_PREFIX = git_

UPRAVDOM_ACCOUNT = factory
UPRAVDOM_QUEUE = ${spooldir}/build-factory

gb_bindir = ${libexecdir}/girar-builder
gb_sbindir = ${sbindir}
gb_confdir = ${sysconfdir}/girar-builder
gb_homedir = ${localstatedir}/girar-builder
gb_initdir = ${sysconfdir}/rc.d/init.d
gb_stopdir = ${gb_confdir}/stop
gb_spooldir = ${spooldir}/girar-builder

GB_HOME = ${gb_sbindir}

bin_auto_TARGETS = \
	gb-sh-conf \
	#

bin_TARGETS = \
	${bin_auto_TARGETS} \
	gb-sh-tmpdir \
	gb-sh-functions \
	gb-sh-rpmhdrcache \
	gb-archive-clean-tasks \
	gb-build-task \
	gb-commit-task \
	gb-local-guard-task \
	gb-local-no-perl58 \
	gb-local-no-py26 \
	gb-task-archive \
	gb-task-build-arch \
	gb-task-build-arch-i \
	gb-task-arepo \
	gb-task-arepo-build \
	gb-task-arepo-build-arch \
	gb-task-arepo-gen-next-repo \
	gb-task-arepo-gen-next-repo-arch \
	gb-task-arepo-gen-repos \
	gb-task-arepo-gen-task-repo \
	gb-task-arepo-gen-task-repo-arch \
	gb-task-arepo-plan \
	gb-task-arepo-plan-arch \
	gb-task-arepo-plan-identify \
	gb-task-arepo-save \
	gb-task-arepo-unmets \
	gb-task-arepo-unmets-arch \
	gb-task-check-acl \
	gb-task-check-build \
	gb-task-check-build-arch \
	gb-task-check-build-i \
	gb-task-check-girar \
	gb-task-check-install \
	gb-task-check-install-arch \
	gb-task-check-lastchange \
	gb-task-check-noarch \
	gb-task-check-noarch-i \
	gb-task-cleanup \
	gb-task-close-bugs \
	gb-task-commit-acl \
	gb-task-commit-arepo \
	gb-task-commit-arepo-arch \
	gb-task-commit-girar \
	gb-task-commit-pocket \
	gb-task-commit-repo \
	gb-task-commit-repo-hash \
	gb-task-copy-packages \
	gb-task-copy-packages-i \
	gb-task-copy-remote-arch-i \
	gb-task-find-closed-bugs \
	gb-task-gen-buildrepo \
	gb-task-gen-ci \
	gb-task-gen-next-repo \
	gb-task-gen-task-repo \
	gb-task-gen-task-repo-skel \
	gb-task-gen-task-repo-skel-arch \
	gb-task-local-policy \
	gb-task-pkgtar \
	gb-task-queue-rebuild \
	gb-task-repo-elfsym \
	gb-task-repo-plan \
	gb-task-repo-unmets \
	gb-task-repo-vercheck \
	gb-task-save-repo \
	gb-task-save-repo-arch \
	gb-task-send-email \
	gb-task-set-summary \
	gb-task-setup-remote \
	gb-task-setup-remote-arch \
	gb-task-show \
	gb-task-validate-state \
	gb-toplevel-build \
	gb-toplevel-commit \
	gb-toplevel-run \
	gb-x-check-rpm-changelogname \
	gb-x-fixup-buildlog \
	gb-x-gen-ci \
	gb-x-girar \
	gb-x-parse-bugs-from-changelog \
	gb-x-rpmaddsign \
	gb-x-rpm-changelog \
	gb-x-rsync-loop \
	gb-x-ssh \
	gb-x-useful-files \
	gb-y-arch-iterator \
	gb-y-arepo-arch-iterator \
	gb-y-arepo-genbasedir-arch \
	gb-y-archive-stale-tasks \
	gb-y-awake-tasks \
	gb-y-calc-max-iter \
	gb-y-deposit-file \
	gb-y-make-git-html-index \
	gb-y-purge-archived-tasks \
	gb-y-repo-regen-basedir \
	gb-y-reposit-task \
	gb-y-select-task \
	gb-y-task-commit-packages \
	#

sbin_auto_TARGETS = \
	gb-clean-tasks \
	gb-init-builder \
	#

sbin_TARGETS = \
	${sbin_auto_TARGETS} \
	#

home_TARGETS = \
	TASK \
	#

remote_TARGETS = \
	remote/gb-remote-arepo-build \
	remote/gb-remote-build \
	remote/gb-remote-check-install \
	remote/gb-remote-copy-packages \
	remote/gb-remote-log \
	remote/gb-remote-plant \
	remote/gb-x-reset-atime \
	remote/gb-x-check-atime \
	#

template_TARGETS = \
	template/git-entry.html.in \
	template/git-footer.html \
	template/git-header-date.html.in \
	template/git-header-name.html.in \
	#

auto_TARGETS: bin_auto_TARGETS sbin_auto_TARGETS

TARGETS = ${bin_TARGETS} ${sbin_TARGETS} ${home_TARGETS} ${remote_TARGETS} ${template_TARGETS} ${auto_TARGETS}

.PHONY: all clean install install-bin install-conf install-data install-home install-remote install-template

all: ${TARGETS}

clean:
	${RM} ${bin_auto_TARGETS}

remote/gb-x-reset-atime:
	make -C remote gb-x-reset-atime

remote/gb-x-check-atime:
	make -C remote gb-x-check-atime

install: install-bin install-sbin install-conf install-template install-home install-remote

install-bin: ${bin_TARGETS}
	install -d -m750 ${DESTDIR}${gb_bindir}
	install -pm755 $^ ${DESTDIR}${gb_bindir}/

install-sbin: ${sbin_TARGETS}
	install -d -m755 ${DESTDIR}${gb_sbindir}
	install -pm750 $^ ${DESTDIR}${gb_sbindir}/

install-home: ${home_TARGETS}
	install -d -m755 ${DESTDIR}${gb_homedir}/bin
	install -pm700 $^ ${DESTDIR}${gb_homedir}/bin/

install-remote: ${remote_TARGETS}
	install -d -m755 ${DESTDIR}${gb_bindir}/remote
	install -d -m775 ${DESTDIR}${gb_bindir}/remote/apt
	install -pm700 $^ ${DESTDIR}${gb_bindir}/remote/

install-template: ${template_TARGETS}
	install -d -m750 ${DESTDIR}${gb_bindir}/template
	install -pm640 $^ ${DESTDIR}${gb_bindir}/template/

install-conf: conf/gb-sh-conf-site girar-builder.init
	install -d -m750 ${DESTDIR}${gb_confdir}
	install -pm640 conf/gb-sh-conf-site ${DESTDIR}${gb_confdir}/
	install -d -m755 ${DESTDIR}${gb_initdir}
	install -pm755 girar-builder.init ${DESTDIR}${gb_initdir}/girar-builder
	install -d -m755 ${DESTDIR}${gb_stopdir}
	install -d -m750 ${DESTDIR}${gb_spooldir}/people

install-perms:
	chown GB_USER:GB_GROUP \
		${DESTDIR}${gb_bindir} \
		${DESTDIR}${gb_confdir} \
		${DESTDIR}${gb_homedir}
	chown :GB_GROUP \
		${DESTDIR}${gb_sbindir}/gb-clean-tasks
	chmod 770  \
		${DESTDIR}${gb_homedir}

%: %.in
	sed -e 's,@EMAIL_DOMAIN@,${EMAIL_DOMAIN},g' \
	    -e 's,@GB_USER@,${GB_USER},g' \
	    -e 's,@GB_GROUP@,${GB_GROUP},g' \
	    -e 's,@GB_HOME_DIR@,${gb_homedir},g' \
	    -e 's,@GB_CONF_DIR@,${gb_confdir},g' \
	    -e 's,@GB_STOP_DIR@,${gb_stopdir},g' \
	    -e 's,@GIRAR_REPOSITORIES@,${GIRAR_REPOSITORIES},g' \
		<$< >$@
	chmod --reference=$< $@
