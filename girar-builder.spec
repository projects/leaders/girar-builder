Name: girar-builder
Version: 0.2
Release: alt1.eter1

Summary: builder part of girar server engine
License: GPL
Group: System/Servers
Packager: Evgeny Sinelnikov <sin@altlinux.ru>

Source: %name-%version.tar

Requires: girar >= 0.3-alt1.eter43
Requires: apt-repo-tools >= 0.6.0.12

Requires(pre): shadow-utils
# due to "enable -f /usr/lib/bash/lockf lockf"
Requires: bash-builtin-lockf >= 0:0.2
# due to post-receive hook
Requires: git-core >= 0:1.5.1
Requires: qa-robot
Requires: inotify-tools
Requires: memcached rpmhdrcache
Requires: monit

BuildRequires: perl(RPM.pm) perl(Date/Format.pm)
BuildRequires: glibc-devel-static

%define gb_user girar-builder
%define gb_group girar-builder
%define gb_homedir %_localstatedir/%name
%define gb_bindir %_usr/libexec/%name
%define gb_runtimedir %_runtimedir/%name

%define gb_keydir %_prefix/lib/%name-gpgkeys

%post
if [ ! -e "%_sysconfdir/%name/repositories" ]; then
    if [ -f "%_sysconfdir/girar/repositories" ]; then
        cp %_sysconfdir/girar/repositories %_sysconfdir/%name/repositories
    else
        touch %_sysconfdir/%name/repositories
    fi
fi
%post_service %name

%preun
%preun_service %name

%pre
if getent passwd %gb_user >/dev/null ; then
    if [ "$(getent passwd %gb_user | cut -d: -f6)" != "%gb_homedir" ]; then
        /usr/sbin/usermod -d %gb_homedir -s /bin/sh %gb_user >/dev/null 2>&1 ||:
    fi
else
    /usr/sbin/useradd -r -g %gb_group -d %gb_homedir -c 'The girar builder' -n %gb_user -s /bin/sh >/dev/null 2>&1 ||:
fi

%description
This package contains %summary.

%prep
%setup -q

%build
%make_build remote/gb-x-reset-atime

%install
%make_install install DESTDIR=%buildroot
touch %buildroot%_sysconfdir/%name/repositories
%add_findreq_skiplist /usr/libexec/%name/remote/*

mkdir -p %buildroot%_sysconfdir/%name/skel
mkdir -p %buildroot%gb_runtimedir
mkdir -p %buildroot%_sysconfdir/rc.d/init.d

install -m 644 functions-gb %buildroot%_sysconfdir/rc.d/init.d/

mkdir -p %buildroot%_sysconfdir/monit.d/
install -m 640 %name.monit %buildroot%_sysconfdir/monit.d/%name

mkdir -p %buildroot%gb_keydir
cat >%buildroot%gb_keydir/gpg.conf <<"_EOF"
no-greeting
lock-never
always-trust
no-secmem-warning
quiet
_EOF

%check
cd tests
./run

%files
%_sysconfdir/rc.d/init.d/functions-gb
%attr(750,root,%gb_group) %_sbindir/gb-*
%defattr(-,root,%gb_group,750)
%gb_bindir/gb-*
%dir %attr(750,%gb_user,%gb_group) %_sysconfdir/%name
%_sysconfdir/%name/gb-*
%dir %_spooldir/%name
%dir %_spooldir/%name/people
%dir %_sysconfdir/%name/stop
%dir %attr(770,root,%gb_group) %gb_runtimedir
%dir %attr(750,%gb_user,%gb_group) %gb_homedir
%dir %attr(750,root,%gb_group) %gb_bindir
%dir %attr(755,root,%gb_group) %gb_bindir/remote
%dir %attr(755,root,%gb_group) %gb_bindir/template
%dir %attr(755,%gb_user,%gb_group) %gb_bindir/remote/apt
%defattr(640,root,%gb_group,750)
%gb_bindir/template/*
%defattr(750,root,%gb_group,750)
%gb_homedir/bin
%gb_bindir/remote/gb-*
%defattr(755,root,root,755)
%_initdir/%name
%config(noreplace) %_sysconfdir/monit.d/%name
%dir %_sysconfdir/%name/skel
%ghost %config(noreplace) %attr(640,%gb_group,root) %_sysconfdir/%name/repositories
%dir %attr(0750,root,%gb_group) %gb_keydir
%attr(0750,root,%gb_group) %gb_keydir/gpg.conf

%changelog
* Thu Feb 28 2013 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter48
- gb-task-commit-repo: fix genbases regress.

* Thu Dec 20 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter47
- Add GB_NPROCS argument to gb-remote-plant and scripts it using.

* Wed Dec 05 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter46
- gb-task-repo-plan: avoid problem with empty plan for genbases.

* Tue Dec 04 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter45
- gb-task-gen-testrepo: avoid problem during removing no exists
  package from bin.list.

* Wed Jul 18 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter44
- gb-x-gen-ci: support empty contents index for empty directory.
- gb-x-useful-files: avoid error on empty directory.
- gb-task-copy-packages-i: fix repo_conf initialization.
- gb-sh-functions: improve make_repo_table error handling with
  pockets.
- gb-task-copy-packages-i: support coping from pockets.
- gb-y-repo-regen-basedir: Turn on using --maybe-unchanged for
  genbasedir due it fixed (ALT#24433).
- Update Makefile for merged version, package build arch is not
  noarch now..
- gb-remote-build: implemented atime-based file usage manifests (thx Alexey Tourbin).
- remote/gb-x-atime.c: initial revision (thx Alexey Tourbin).
- Implement "copy" build method for arepoized packages (thx Dmitry V. Levin).
- gb-build-task: run gb-task-arepo before gb-task-gen-ci (thx Dmitry V. Levin).
- gb-y-awake-tasks: use grep instead of sed (thx Dmitry V. Levin).
- gb-task-arepo-plan-arch: change report wording (thx Dmitry V. Levin).
- gb-task-arepo-plan-arch: optimize "no changes" case (thx Dmitry V. Levin).
- gb-task-arepo-plan-arch: update arepo/$arch/done.list as well (thx Dmitry V. Levin).
- gb-task-repo-plan: fix typo (thx Dmitry V. Levin).
- gb-task-commit-repo: tweak diagnostics (thx Dmitry V. Levin).
- gb-commit-task: call gb-task-commit-arepo right after
  gb-task-commit-repo (thx Dmitry V. Levin).
- gb-y-repo-regen-basedir: parametrize --bz2/--no-bz2 genbasedir
  option using $GB_REPO_OPT_BZ2 (thx Dmitry V. Levin).
- Implement debuginfo support (thx Dmitry V. Levin).
- gb-y-purge-archived-tasks: fix typo (thx Dmitry V. Levin).
- Merge plan/i-{src,bin} into plan/add-{src,bin} (thx Dmitry V. Levin).
- gb-task-commit-meta: remove unused file (thx Dmitry V. Levin).
- gb-task-arepo-build-arch: do not check python policy (thx Dmitry V. Levin).
- gb-task-arepo-build-arch: additionally check the result (thx Dmitry V. Levin).
- Introduce plan/i-{src,bin} (thx Dmitry V. Levin).
- gb-task-build-arch-i: do not sign srpm files unnecessary (thx Dmitry V. Levin).
- Move plan cleanup from gb-task-archive to gb-task-cleanup (thx Dmitry V. Levin).
- gb-task-cleanup: remove more arepo stuff (thx Dmitry V. Levin).
- gb-task-repo-plan: output plan summary (thx Dmitry V. Levin).
- gb-task-copy-packages-i: do not use *RPMS.classic, refer to
  packages directly via files/SRPMS/ and files/$arch/RPMS/ (thx Dmitry V. Levin).
- make_repo_table: in case of bootstrap, use files/SRPMS/ and
  files/$arch/RPMS/ directly (thx Dmitry V. Levin).
- Parametrize arepo component name using $GB_AREPO_COMPONENT_NAME (thx Dmitry V. Levin).
- gb-task-save-repo-arch: drop pkglist.classic.bz2 from the test (thx Dmitry V. Levin).
- gb-task-commit-repo: use pkglist.classic.xz instead of
  pkglist.classic.bz2 for comparison (thx Dmitry V. Levin).
- gb-task-check-install-arch: do not copy any kind of bzipped lists
  to the remote host (thx Dmitry V. Levin).
- gb-task-arepo-plan-identify: sync GB_AREPO_LIB_FILE_REGEXP with
  rpmrebuild-arepo 3.1.1-alt1 (thx Dmitry V. Levin).
- Change arepo arch naming scheme from compat-native to native-compat (thx Dmitry V. Levin).
- Implement arepo support (thx Dmitry V. Levin).
- Introduce plan/change-arch (thx Dmitry V. Levin).
- gb-y-repo-regen-basedir: tweak text arguments passed to genbasedir (thx Dmitry V. Levin).

* Thu Jan 26 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter43
- gb-task-commit-repo: revert no set owner,group,perms and times
  for rsync from tempdir.

* Sat Jan 21 2012 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter42
- Require new girar supported new features.
- gb-task-gen-testrepo: copy with no-preserve=ownership due sshfs troubles with it.
- gb-purge-archived-tasks: fix purged task logs.
- gb-local-no-py26: fix deps matching pattern (thx Dmitry V. Levin).
- gb-local-no-py26: enhance diagnostics (thx Dmitry V. Levin).
- gb-build-task: allow swift tasks to complete (thx Dmitry V. Levin).
- gb-build-task: disable gb-sh-rpmhdrcache (thx Dmitry V. Levin).
- gb-build-task: use task/run (thx Dmitry V. Levin).
- gb-task-build-arch-i: fix a typo in diagnostics (thx Dmitry V. Levin).
- gb-task-build-arch-i: add task/abort support (thx Dmitry V. Levin).
- gb-task-build-arch-i: copy some build warnings from the build
- gb-y-awake-tasks: use tmp files, "sed -i" is not available because
  of permissions (thx Dmitry V. Levin).
- gb-task-queue-rebuild: do not bump iteration counter unless the
  latest iteration was nondegenerate (thx Dmitry V. Levin).
- Implement tasks awakening and postponing (thx Dmitry V. Levin).
- gb-y-archive-stale-tasks: bump timeout (thx Dmitry V. Levin).
- gb-y-archive-stale-tasks: track task/state instead of task (thx Dmitry V. Levin).
- Add cancellation points for task/abort (thx Dmitry V. Levin).
- gb-task-check-noarch-i (dump_rpmfile): ignore DBase size
  differences (thx Dmitry V. Levin).
- gb-task-check-noarch-i (dump_rpmfile): ignore GIF image size (thx Dmitry V. Levin).
- gb-task-check-noarch-i (dump_rpmfile): pass -n option to rpmpeek (thx Dmitry V. Levin).
  log to the task log (thx Dmitry V. Levin).
- gb-task-check-install-arch: add task/abort support (thx Dmitry V. Levin).
- gb-task-check-install: conditionally tolerate install check
  failures (thx Dmitry V. Levin).
- gb-task-check-build-i: print a broad note in case when
  sisyphus_check failure is tolerated (thx Dmitry V. Levin).
- gb-task-check-build-i: conditionally tolerate sisyphus_check
  failures (thx Dmitry V. Levin).
- gb-task-{check,commit}-girar: use git_get_branch_id() (thx Dmitry V. Levin).
- gb-sh-functions (git_get_branch_id): new function (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_commit_srpm): ignore gear-srpmimport
  errors (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_commit_copy): treat missing source
  branch as a srpm upload (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_commit_copy): do not complain about
  missing git repository (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_rename_repo_branch): Be more verbose
  about saving old branch (thx Dmitry V. Levin).
- gb-task-commit-girar: save old branch in case of broken inheritance (thx Dmitry V. Levin).
- gb-task-commit-girar: do not remove obsolete srpm branches (thx Dmitry V. Levin).
- gb-task-commit-girar: update girar_obsolete() usage (thx Dmitry V. Levin).
- gb-task-commit-girar: s/bypass/relax/ (thx Dmitry V. Levin).
- gb-task-commit-girar: add more information about git inheritance
  check bypass to the log (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_obsolete): make "git branch -D"
  more silent (thx Dmitry V. Levin).
- gb-task-commit-girar: remove obsolete srpm branch in case of
  committing gear update (thx Dmitry V. Levin).
- gb-task-commit-girar: remove obsolete gear branch in case of
  committing srpm update (thx Dmitry V. Levin).
- gb-task-check-girar (girar_check_copy): treat missing source
  branch as a srpm upload (ALT#26573) (thx Dmitry V. Levin).
- gb-task-commit-girar: robustify use of git-rev-parse (thx Dmitry V. Levin).
- gb-task-commit-girar: append task id to the old branch name (thx Dmitry V. Levin).
- gb-commit-task: disable gb-sh-rpmhdrcache (thx Dmitry V. Levin).
- gb-remote-build: replace sha1sum with a cheaper test (thx Dmitry V. Levin).
- gb-task-gen-buildrepo: fix GB_TMP_REPO_DIR using after merge.
- Move hasher build repo outside of workdir (thx Dmitry V. Levin).
- Use gb-x-ssh (thx Dmitry V. Levin).
- gb-x-ssh: new helper (thx Dmitry V. Levin).
- Update python deps prohibition for python2.6. (thx Dmitry V. Levin).
- Implement task/swift (thx Dmitry V. Levin).
- gb-task-gen-ci, gb-task-repo-elfsym, gb-y-repo-regen-basedir:
  enable gb-sh-rpmhdrcache (thx Dmitry V. Levin).
- gb-task-check-lastchange (check): try also UTC+-24 to workaround
  TZ impact (thx Dmitry V. Levin).
- gb-task-check-lastchange: round CHANGLOGTIME days, to suppress
  TZ impact (thx Alexey Tourbin).
- remote/gb-remote-check-install: pass required mountpoints to rpm -e
  (ALT#26470) (thx Dmitry V. Levin).
- Fix copy if any source archs is absent in target repository.
- Support for repositories without files directory.
- gb-task-check-girar (fail, warn): do not prepend the stamp (thx Dmitry V. Levin).
- gb-task-check-girar (girar_check_srpm): add inhertitance bypass
  support (thx Dmitry V. Levin).
- gb-task-check-build-i: save the name of the built source package (thx Dmitry V. Levin).
- gb-task-check-lastchange: update for the case of bypassed git
  inheritance check (thx Dmitry V. Levin).
- gb-task-check-girar: implement bypass mechanism for git inheritance
  check (thx Dmitry V. Levin).
- gb-task-check-girar: robustify use of git-rev-parse (thx Dmitry V. Levin).
- gb-task-check-girar: output COND-OK instead of OK in case of
  cond_fail (thx Dmitry V. Levin).
- gb-task-check-lastchange: robustify use of git-rev-parse (thx Dmitry V. Levin).
- add group for monit rules.
- gb-task-send-email: skip tasks with "skip" flag set (thx Dmitry V. Levin).
- gb-task-send-email: add X-girar-task-log (thx Dmitry V. Levin).
- gb-task-send-email: do not notify unmet-addressees for test-only
  tasks (thx Dmitry V. Levin).
- TASK: update (thx Dmitry V. Levin).
- Relocate some files to new subdirs logs/ and report/. (thx Dmitry V. Levin).
- gb-toplevel-commit: include $min_iter to the log message (thx Dmitry V. Levin).
- Merge gb-y-repo-regen-ci into gb-task-gen-ci (thx Dmitry V. Levin).
- Generate contents indices and save test repo at the end of
  build stage (thx Dmitry V. Levin).
- gb-task-commit-repo: remove contents_index files generation (thx Dmitry V. Levin).
- gb-task-gen-testrepo: generate new contents_index files (thx Dmitry V. Levin).
- gb-y-repo-regen-ci: fail if at least one arch failed (thx Dmitry V. Levin).
- gb-task-commit-repo: remove tmpfs paranoia check (thx Dmitry V. Levin).
- gb-task-save-repo-arch: add tmpfs paranoia check (thx Dmitry V. Levin).
- Rename gb-clone-repo-dir -> gb-task-clone-repo-dir (thx Dmitry V. Levin).
- Rename gb-select-task -> gb-y-select-task (thx Dmitry V. Levin).
- Rename gb-purge-archived-tasks -> gb-y-purge-archived-tasks (thx Dmitry V. Levin).
- Rename gb-make-git-html-index -> gb-y-make-git-html-index (thx Dmitry V. Levin).
- Rename gb-calc-max-iter -> gb-y-calc-max-iter (thx Dmitry V. Levin).
- Rename gb-archive-stale-tasks -> gb-y-archive-stale-tasks (thx Dmitry V. Levin).
- Rename gb-girar -> gb-x-girar (thx Dmitry V. Levin).
- Rename check-rpm-changelogname -> gb-x-check-rpm-changelogname (thx Dmitry V. Levin).
- Rename rsync_forever -> gb-x-rsync-loop (thx Dmitry V. Levin).
- Rename rpmaddsign -> gb-x-rpmaddsign (thx Dmitry V. Levin).
- gb-task-repo-elfsym: report bad_elf_symbols_dircmp.pl failure (thx Dmitry V. Levin).
- gb-make-git-html-index: print date using %F format (thx Dmitry V. Levin).
- gb-make-git-html-index: use highlight alternation (thx Dmitry V. Levin).
- template/git-header.html.in: remove obsolete file (thx Dmitry V. Levin).
- gb-purge-archived-tasks: log each purged task (thx Dmitry V. Levin).
- rsync_forever: set timeout to 1 hour (thx Dmitry V. Levin).
- gb-purge-archived-tasks: purge old eperm tasks as well (thx Dmitry V. Levin).
- gb-task-send-email: notify groups as well (thx Dmitry V. Levin).
- gb-sh-rpmhdrcache: switch to rpmhdrmemcache (thx Dmitry V. Levin).

* Sat Oct 01 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter41
- Fix init-scripts for builder and commiter.
- Prepare to use common directory for girar-builder gpg keys.
- Set GB_ADDITIONAL_FLAVOURS to be optional.
- Add monit template for builders an commiter restart.
- Add directory for builder users home skel.
- Remove old template cron file for girar-builder.
- Update initialization script for builder.

* Sun Jul 31 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter40
- Add first initial girar-builder script gb-init-builder
- Add start/stop/restart commands for builder and commiter to init script

* Fri Jun 24 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter39
- Force create index directories for building and committing.

* Wed Jun 08 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter38
- Fix condstop and condrestart init script logic.
- gb-task-setup-remote-arch: avoid races during sources copying.
- Add missed  for gb-sh-conf-* files including.

* Wed Jun 08 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter37
- Add GB_LIST_OF_CHECKS_ALLOWED_TO_FAIL option to template config.
- gb-task-check-girar (girar_check_copy): ask
  is_check_failure_tolerated() whether to tolerate check_inheritance()
  failures (thx Dmitry V. Levin).
- gb-task-repo-unmets: do not send notifications about unmets in
  just built packages (thx Dmitry V. Levin).
- gb-remote-build: Adjust ExcludeArch/ExclusiveArch handle with
  new error log.
- Add GB_ACL_ADDON option to template options list.
- gb-task-check-acl: save acl check data into plan/check_acl (thx Dmitry V. Levin).
- gb-task-commit-acl: parametrize @everybody using $GB_ACL_ADDON (thx Dmitry V. Levin).
- gb-sh-conf: add GB_ACL_ADDON (thx Dmitry V. Levin).
- gb-build-task: recognize EPERM state (thx Dmitry V. Levin).
- gb-task-check-acl: change unsuccessful exit code from 1 to 7 (thx Dmitry V. Levin).

* Mon May 23 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter36
- Multiple support fixes
- Relace remote scripts to gb_bindir/remote
- Check empty HOME and SHELL for girar-builder user
- Add repositories list as ghost file

* Fri May 20 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter35
- Fix last merge problems
- Add require for rpmhdrcache due gb-sh-rpmhdrcache

* Wed Apr 27 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter34
- Multiple changes requires new girar interface for task control

* Wed Dec 08 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter33
- Add crutch for remote builder with .hasher/config.local file, using
  for additional hasher options, like 'export share_network=1' (Eter#6603)

* Mon Nov 22 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter32
- Fix gb-task-check-lastchange for auto release build

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter31
- Skip ACL checks for pockets

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter30
- Skip auto release builds checking with gb-task-check-lastchange

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter29
- Revert turn off gb-task-repo-elfsym, because bad_elf_symbols_dircmp.pl
  not usable for multiples rpm directories needs for addon repositories

* Tue Nov 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter28
- Support girar commit with branch_id using instead of tag_name
- Fix using gb-x-useful-files and bad_elf_symbols_dircmp.pl with "no rpms" error
  (find with '-empty' option needs last slash for directory)

* Wed Oct 27 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter27
- Revert using GB_ADDITIONAL_FLAVOURS instead of internal GB_REPO_COMPONENT_NAMES
- Turn off using --maybe-unchanged option for genbasedir until it not be fixed with
  multiple components (ALT#24433)
- Refix problem with checksum for pkglist of additional flavour (Eter#6302)

* Tue Oct 26 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter26
- Fix problem with repo rsync after success building

* Thu Oct 21 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter25
- Fix genbases with additional checks.

* Mon Oct 18 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter24
- Add cron file for tasks archive and clean.
- Merge with last updates and fixes:
- gb-local-setv: diagnose not-yet set-versioned dependencies (thx Alexey Tourbin).
- gb-run-task: if the task is shared, mention this fact in the log (thx Dmitry V. Levin).
- Remove redundant env(1) invocations (thx Dmitry V. Levin).
- gb-select-task: skip tasks without non-empty subtasks (thx Dmitry V. Levin).
- Generate apt indices for the task repository (ALT#23934) (thx Dmitry V. Levin).
- gb-sh-conf: make GB_REPO_LABEL optional (thx Dmitry V. Levin).
- gb-y-repo-regen-basedir: make genbasedir options more configurable (thx Dmitry V. Levin).
- Generate rpm-dir for built and partially validated packages
  (ALT#23934) (thx Dmitry V. Levin).
- gb-run-task: call girar-make-task-index-html (thx Dmitry V. Levin).
- gb-run-task: mention the name of account that initiated the build (thx Dmitry V. Levin).
- Rename gb-make-html-index -> gb-make-git-html-index (thx Dmitry V. Levin).
- gb-task-check-acl: don't stamp girar-check-perms output (thx Alexey Tourbin).
- gb-y-repo-regen-basedir: pass --maybe-unchanged option to
  genbasedir (thx Alexey Tourbin).
- gb-task-check-noarch-i: extra check for -m32 packages like cpuburn (thx Alexey Tourbin).
- gb-purge-archived-tasks: purge old tested tasks as well (thx Dmitry V. Levin).
- gb-task-commit-acl: pass --quiet option to girar-acl (thx Dmitry V. Levin).

* Tue Jul 13 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter23
- Fixed last merge problem with missed gb-task-check-lastchange

* Tue Jul 13 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter22
- Fixed init_pocket with self sources.
- Merge with last updates and fixes (thx Dmitry V. Levin):
+ Initialize task/acl-addressees early.
+ gb-task-send-email: add ACL members to Cc list.
+ gb-task-check-acl: save the list of ACL members.

* Thu Apr 01 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter21
- Fixed pockets with GB_REPO_SELF_SOURCES using
- Set check for GB_REPO_SOURCES GB_REPO_SELF_SOURCES
- Fixed girar-builder-run script

* Wed Mar 31 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter20
- Add system script gb-clean-tasks for clean old tasks:
+ gb-task-send-email: Fixed for special emails
+ girar-builder-run: Add for run gb-commands
- Fixed girar-builder for CWD detecting

* Tue Mar 09 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter19
- Fixed merged installation problems

* Sun Feb 28 2010 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter18
- Merge with last updates and fixes (thx Dmitry V. Levin):
+ gb-task-archive: Remove large plan/next.* files
+ Hook up a general archive cleanup after each build
+ gb-select-task: Obtain a shared lock on the whole TASKS directory
+ gb-run-task: be more verbose about acquiring repository lock
+ Rename: task/manual -> task/test-only
+ Log task completion
+ gb-task-commit-acl: Add @everybody to the tail
+ gb-task-commit-girar (girar_commit_git): save space by replacing local
  objects store with symlink to global store

* Mon Dec 14 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter17
- Fix no one mirror sync error

* Tue Nov 10 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter16
- Reduce build loop interval from 5 seconds to 12.
- Comment halt, when sync to mirrors failed.
- Merge with last updates and fixes:
+ gb-task-commit-acl: Handle robot users (thx Dmitry V. Levin).
+ gb-task-commit-acl: Make the whole command disablable using
  GB_DISABLE_ACL_UPDATE variable (thx Dmitry V. Levin).
+ gb-remote-build, gb-remote-check-install: Drop redundant setarch
  calls (thx Dmitry V. Levin).
+ gb-remote-check-install: Use --rooter option spelling consistently (thx Dmitry V. Levin).
+ gb-remote-plant: Override system apt pkgpriorities (thx Dmitry V. Levin).
+ remote/gb-remote-plant: Add /dev/pts to known_mountpoints (thx Dmitry V. Levin).
+ Set vendor to packages email.
+ LICENSE: GPLv2+ (thx Alexey Tourbin).

* Sun Sep 27 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter15
- Merge with last updates and fixes
+ remote/gb-remote-plant: Add /dev/pts to known_mountpoints (thx Dmitry V. Levin).
+ Move queue halting code to separate function (thx Dmitry V. Levin).
+ gb-task-repo-elfsym: Turn off global cutout switch in case of ENOMEM
  in bad_elf_symbols_dircmp.pl (thx Dmitry V. Levin).
+ gb-task-cleanup: Cleanup task by removing intermediate build results
  (thx Alexey Tourbin).
+ gb-task-gen-buildrepo: s/consequent/consecutive/ (thx Alexey Tourbin).
+ gb-y-repo-regen-ci: updated for gb-x-gen-ci and made it parrallel
  (thx Alexey Tourbin).
+ gb-x-gen-ci: new helper (thx Alexey Tourbin).

* Tue Aug 25 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter14
- fix pockets support with using right gears directory and saving plan

* Tue Aug 25 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter13
- add support for pockets build

* Thu Aug 20 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter12
- add support for regenerate basedirs for apt.

* Wed Jul 08 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter11
- Fix using sshfs on GB_REPO_DIR
+ gb-y-repo-regen-ci: copy contents_index instead move it due need
  for set owner.
+ gb-task-commit-repo: set no owner,group,perms and times for rsync
  from tempdir.

* Wed Jul 08 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter10
- Update from master branch and adapt for common using
+ gb-y-repo-regen-basedir: per-arch parallel execution (thx Alexey Tourbin).
+ gb-task-gen-buildrepo: fixed need_regen test (thx Alexey Tourbin).
+ gb-run-task: Move gb-task-check-acl to the end of
  check list (thx Dmitry V. Levin).
+ gb-task-commit-acl: Optimize a bit (thx Dmitry V. Levin).
+ gb-run-task: lock buildrepo before gen-buildrepo (thx Alexey Tourbin).
+ gb-task-check-install-arch: less rsync to remote (thx Alexey Tourbin).
+ gb-task-setup-remote-arch: less rsync to remote (thx Alexey Tourbin).
+ gb-task-setup-remote-arch: enabled buildrepo (thx Alexey Tourbin).
+ gb-task-gen-buildrepo: new stage, to deal with file-level
  dependencies (thx Alexey Tourbin).
+ gb-task-gen-testrepo: updated stamp message (thx Alexey Tourbin).
+ gb-task-tmprepo -> gb-task-gen-testrepo (thx Alexey Tourbin).
+ gb-y-repo-regen-basedir: added --bloat option (thx Alexey Tourbin).

* Wed Jun 17 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter9
- add qa-robot dependence for gb-task-repo-unmets
- gb-remote-check-install: Fix warning wording (thx Dmitry V. Levin).
- gb-task-pkgtar: Set TZ=UTC when creating pkg.tar (thx Dmitry V. Levin).
- gb-make-html-index (mkidx): Substitute @SITE@ (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_obsolete): Fix git-branch use (thx Dmitry V. Levin).
- Forward task owner and repo names to remote builder for use
  in hasher (thx Dmitry V. Levin).
- gb-task-commit-girar (girar_commit_git, girar_commit_srpm):
  Ignore git-repack errors (thx Dmitry V. Levin).
- gb-task-send-email: Add Reply-To: entry to email header (thx Dmitry V. Levin).
- gb-remote-check-install: optimize whatprovides check (thx Dmitry V. Levin).
- Pass $GB_REPO_NAME to gb-remote-check-install (thx Dmitry V. Levin).
- gb-remote-check-install: Workaround conflicting altlinux-release
  providers (thx Dmitry V. Levin).
- Pass arch and filename to gb-remote-check-install (thx Dmitry V. Levin).
- gb-sh-functions (vercheck): Fix rpmevrcmp output check (thx Dmitry V. Levin).
- gb-remote-log: added "Backtrace:" pattern (thx Alexey Tourbin).


* Wed May 06 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter8
- gb-y-repo-regen-basedir: add GB_ADDITIONAL_FLAVOURS using with result genbasedir.
- add force package removing then it absent.
- gb-remote-check-install: fixup for pkg-config (thx Alexey Tourbin).

* Thu Apr 30 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter7
- fix GB_CONF_DIR for gb-sh-conf-$REPO includes

* Wed Apr 29 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter6
- fixed rpmevrcmp for not sequence versions.
- gb-remote-check-install: added /var/cache/fontconfig and
  /var/resolv (thx Alexey Tourbin).
- gb-remote-check-install: switch to --whatprovides, to accomodate
  alternatives (thx Alexey Tourbin).
- gb-remote-check-install: ignore well-known files not owned by
  any package (thx Alexey Tourbin).
- gb-remote-plant: added Debug::pkg* options to apt.conf (thx Alexey Tourbin).
- gb-task-check-install-arch: prevent stdin leak (thx Alexey Tourbin).
- gb-task-send-email: Lower log file size limit (thx Dmitry V. Levin).
- genbasedir now has --cachedir=DIR option (thx Alexey Tourbin).
- gb-y-repo-regen-basedir: enabled cross-arch useful-files mode (thx Alexey Tourbin).
- gb-task-tmprepo: fixed SRPMS.classic symlinks (thx Alexey Tourbin).
- gb-task-tmprepo: re-create basedirs after removal (thx Alexey Tourbin).
- gb-sh-functions (vercheck): Remove workaround for rpmevrcmp (thx Dmitry V. Levin).
- gb-task-tmprepo: restored custom packages commit (thx Alexey Tourbin).
- gb-task-check-install-arch: reverted rsync --delete (thx Alexey Tourbin).
- gb-task-tmprepo: explicitly remove basedirs, to avoid symlinks (thx Alexey Tourbin).
- gb-task-commit-repo: reuse tmprepo basedirs, handle tmpfs failures (thx Alexey Tourbin).
- gb-task-tmprepo: remove contents_index as well (thx Alexey Tourbin).
- gb-task-check-install-arch: rsync --delete (thx Alexey Tourbin).
- gb-task-tmprepo: made a separate stage (thx Alexey Tourbin).
- gb-y-task-commit-packages: last commit-repo helper (thx Alexey Tourbin).
- gb-y-repo-regen-ci: only regen when $arch packages affected (thx Alexey Tourbin).
- gb-y-repo-regen-basedir, gb-y-repo-regen-ci: new helpers for
  commit-repo (thx Alexey Tourbin).
- gb-x-useful-files: new helper for genbasedir (thx Alexey Tourbin).
- gb-task-close-bugs: Optimize out $repo variable (thx Dmitry V. Levin).
- gb-task-close-bugs: restored $repo var (thx Alexey Tourbin).
- gb-run-task, gb-task-close-bugs: moved to "apply changes" section (thx Alexey Tourbin).
- gb-remote-check-install: mkdir /usr/share/man/ru/man1 befor flist1 (thx Alexey Tourbin).
- gb-remote-check-install: pkg-build-list=basesystem,glibc-locales (thx Alexey Tourbin).
- gb-remote-check-install: post-install filelist check (thx Alexey Tourbin).
- gb-task-check-build-arch: Check for source package duplicates (thx Dmitry V. Levin).

* Wed Apr 22 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter5
- add new fixes and improvements
- fixed noarch check for order.
- add builder loop and SystemV init script.
- fix builder for new features.
- gb-task-close-bugs: Obfuscate email address (thx Dmitry V. Levin).
- gb-x-parse-bugs-from-changelog: Extend regexps to handle optional
  author name finished by semicolon (thx Dmitry V. Levin).
+ gb-task-send-email: Incapsulate attachment into email body (thx Dmitry V. Levin).
+ gb-task-close-bugs: Fix sending mail by replacing mutt with
  sendmail (thx Dmitry V. Levin).
+ Add the task owner to mail headers, for "no such bug" reply
  messages. (thx Mikhail Gusarov).
+ Style fix (thx Mikhail Gusarov).
+ Reusing plan/* instead of parsing src.rpm/rpm (thx Mikhail Gusarov).
+ Adjusting find(1) usage to avoid ugly egrep (thx Mikhail Gusarov).
+ Adding X-Bugzilla-In-* headers to ease processing on Bugzilla side (thx Mikhail Gusarov).
+ Expanding testsuite for new (alt bug nnn) syntax (thx Mikhail Gusarov).
+ Adding alternate bug-closing syntax "(alt bug #nnnnn)" (thx Mikhail Gusarov).
+ Removing unused variable (thx Mikhail Gusarov).
+ Trivial testsuite for gb-x-parse-bugs-from-changelog (thx Mikhail Gusarov).
+ Bug-closing email, initial implementation (thx Mikhail Gusarov).
+ gb-task-send-email: Implement Cc (ALT#19204) (thx Dmitry V. Levin).
+ gb-task-send-email: Include log URL to the body, do not attach
  too large files (thx Dmitry V. Levin).
+ remote/gb-remote-build: If source is src.rpm, examine ExcludeArch
  and ExclusiveArch early (thx Dmitry V. Levin).
+ gb-task-copy-packages: Remove previously copied packages if any (thx Dmitry V. Levin).

* Sat Mar 28 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter4
- fixed new files installation
- fixed GB_REPO_FLAVOUR for remote builder

* Sat Mar 28 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter3
- added GB_REPO_FLAVOUR support
- comment hard check tasks
- fixed copy when hardlinks not admisslible
- Update from master branch
+ Rework packages copying support
   Copied binary packages may be needed for build subtasks,
   so copy them to remote side.
+ gb-run-task: Swap gb-task-build and gb-task-copy-packages

* Tue Mar 10 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter2
- Installing build user
- Replace directories

* Tue Mar 10 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1-alt1.eter1
- Initial revision.

